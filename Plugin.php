<?php namespace Bitcraft\Pagebuilder;

use Bitcraft\Pagebuilder\Models\Setting;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
    }

    public function registerSettings()
    {
    }

    public function register()
    {
    }

    public function boot()
    {
        // create default deployment model
        if (Setting::all()->count() === 0) {
            $settings = new Setting();
            $settings->title = 'Default';
            $settings->save();
        }
    }

    public function registerMarkupTags()
    {
        return [
            'filters' => [
                // A local method, i.e $this->makeTextAllCaps()
                'reduce_webp' => [$this, 'makeImageReduceWebp'],
                'reduce_img' => [$this, 'makeImageReduceImg'],
                'image_type' => [$this, 'makeImageType'],
            ]
        ];
    }

    public function makeImageReduceWebp($url, $size = null)
    {

        $regex = '/^[a-z0-9]{1,6}x[a-z0-9]{1,6}$/';
        if (isset($size) && preg_match($regex, $size)) {
            $url = str_replace('/media/', "/media/sizes/$size/", $url);
        }
        $url = str_replace(['.png','.jpg'], '.webp', $url);

        return $url;
    }

    public function makeImageReduceImg($url, $size = null)
    {

        $regex = '/^[a-z0-9]{1,6}x[a-z0-9]{1,6}$/';
        if (isset($size) && preg_match($regex, $size)) {
            $url = str_replace('/media/', "/media/sizes/$size/", $url);
        }

        return $url;
    }

    public function makeImageType($url)
    {
        $chunks = explode('.', $url);
        return end($chunks);
    }
}
