<?php namespace Bitcraft\Pagebuilder\Controllers;

use Backend\Classes\Controller;
use Backend\Facades\BackendAuth;
use BackendMenu;
use Bitcraft\Publish\Jobs\UploadPages;
use Bitcraft\Pagebuilder\Models\Page;
use Bitcraft\Pagebuilder\Models\Setting;
use Bitcraft\Publish\Models\Platform;
use Bitcraft\Versions\Models\Version;
use Illuminate\Support\Facades\Queue;
use Symfony\Component\HttpFoundation\Request;
use RainLab\Translate\Models\Locale;
use Cms\Classes\Controller as CMSController;
use Cms\Classes\Page as CMSPage;

class Pages extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Bitcraft.Versions.Behaviors.VersionsControllerBehavior',
        'Backend\Behaviors\RelationController',
        'Bitcraft.Publish.Behaviors.PublishControllerBehavior'
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $relationConfig = 'config_relation.yaml';
    public $versionsConfig = 'config_versions.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Bitcraft.Pagebuilder', 'main-menu-item-pagebuilder', 'side-menu-item-pages');
    }

    public function show(Request $request, $default_slug = null, $default_lang = null)
    {
        $cmsController = new CMSController();
        $path = $request->getPathInfo();
        $lang = $default_lang ?? mb_substr($path, 1, 3);
        $locales = array_keys(BackendAuth::check() ? Locale::listAvailable() : Locale::listEnabled());
        $old_version = false;
        $langs = array_map(function ($lang) {
            return "$lang/";
        }, $locales);
        $home_slugs = array_map(function ($lang) {
            return "/$lang";
        }, $locales);

        if ($default_lang !== null || in_array($lang, $langs, true) || in_array($path, $home_slugs, true)) {
            $path = "/".str_replace("/$lang", '', $path);
            $path = str_replace('//', '/', $path);

            if (empty($path)) {
                $path = '/';
            }
        } else {
            $lang = explode('_', $request->getPreferredLanguage())[0];
            if (!in_array($lang, $locales)) {
                $lang = Locale::getDefault()->code;
            }

            if (empty($path)) {
                $path = '/';
            }
            return redirect("/$lang$path");
        }

        $lang = mb_substr($lang, 0, 2);
        if ($request->mode === 'preview'
            && BackendAuth::check()
            && $page = Page::transWhere('slug', $path, $lang)->first()) {
            $page->translateContext($lang);
            $page->noFallbackLocale();
            $modules = $page->getAttributeTranslated('modules', $lang);
        } elseif (($page_id = $request->model) && ($version = $request->version) && BackendAuth::check()) {
            $page = Version::getModelWithVersion($page_id, $version, $lang, Page::class);

            if ($page === null) {
                return $this->pageNotFound($request, $lang);
            }

            $modules = $page->modules;
            $old_version = true;
        } elseif ($page = Page::transModelWhere($path, $lang)) {
            if (!$page->hasPublishedVersion) {
                return $this->pageNotFound($request, $lang);
            }
            $modules = $page->modules;
        } else {
            return $this->pageNotFound($request, $lang);
        }

        if (!$page->published && !BackendAuth::check()) {
            return $this->pageNotFound($request, $lang);
        }

        if (!isset($modules)) {
            return $this->pageNotFound($request, $lang);
        }
        if ($page->slug != $path) {
            return $this->pageNotFound($request, $lang);
        }

        $cms_page = CMSPage::load($cmsController->getTheme(), "page");
        $alternate = [];
        $locale_links = [];
        if (!$old_version) {
            $locale_versions = $page->currentVersion()->locale_versions ?? [];
            $locales = array_keys(Locale::listEnabled());

            foreach ($locales as $locale) {
                if (array_key_exists($locale, $locale_versions)) {
                    $slug = $locale_versions[$locale];
                    $slug = $slug === '/' ? '' : $slug;
                    $link = "{$request->getBaseUrl()}/$locale$slug";
                    if (Locale::getDefault()->code == $locale) {
                        $alternate[] = ['href' => $link, 'hreflang' => 'x-default'];
                    }
                    $alternate[] = ['href' => $link, 'hreflang' => $locale];
                    $locale_links[strtoupper($locale)] = $link;
                } else {
                    $locale_links[strtoupper($locale)] = "{$request->getBaseUrl()}/$locale";
                }
            }
        }


        $seo = $page->seo_tag;
        if ($seo !== null) {
            $seo->noFallbackLocale();
        }

        $settings = $page->platform !== null ? $page->platform->settings->lang($lang) : Setting::find(1)->lang($lang);
        $cmsController->getRouter()->setParameters([
            'page' => $page,
            'menu' => $page->menu !== null
                ? @$page->menu->noFallbackLocale()->lang($lang) : @$settings->menu->noFallbackLocale(),
            'footer' => $page->footer !== null
                ? @$page->footer->noFallbackLocale()->lang($lang) : $settings->footer->lang($lang),
            'modules' => $page->modules,
            'seo' => $seo,
            'path' => $request->getPathInfo(),
            'alternate' => $alternate,
            'locale_links' => $locale_links,
            'current_locale' => strtoupper($lang),
            'settings' => $settings,
            'platform' => $page->platform,
        ]);

        return $cmsController->runPage($cms_page);
    }

    public function pageNotFound(Request $request, $lang)
    {
        $cmsController = new CMSController();
        $cms_page = CMSPage::load($cmsController->getTheme(), "404");
        $settings = Setting::find(1)->lang($lang);

        $locales = array_keys(Locale::listEnabled());
        $locale_links = [];

        foreach ($locales as $locale) {
            $locale_links[strtoupper($locale)] = "{$request->getBaseUrl()}/$locale";
        }

        $cmsController->getRouter()->setParameters([
            'settings' => $settings,
            'menu' => $settings->menu->noFallbackLocale(),
            'footer' => $settings->footer,
            'locale_links' => $locale_links,
            'platform' => Platform::find(1),
            'current_locale' => strtoupper($lang),
        ]);

        return $cmsController->runPage($cms_page);
    }

    public function onDuplicate()
    {
        foreach (post('checked') as $id) {
            if ($page = Page::find($id)) {
                $page->duplicate();
            }
        }
        \Flash::success('Pages duplicated!');
        return back();
    }

    public function onPublishToCloud()
    {
        Queue::push(UploadPages::class);
        \Flash::success('Pages published!');
    }

    public function onCopyLanguageModules($id)
    {
        $locales = explode(' -> ', strtolower(post('mode')));
        if (count($locales) === 2 && $page = Page::find($id)) {
            if ($locales[1] === Locale::getDefault()->code) {
                $page->modules = $page->getAttributeTranslated('modules', $locales[0]);
            } else {
                $page->setAttributeTranslated(
                    'modules',
                    $page->getAttributeTranslated('modules', $locales[0]),
                    $locales[1]
                );
                $page->setAttributeTranslated(
                    'slug',
                    $page->getAttributeTranslated('slug', $locales[1]),
                    $locales[1]
                );
            }

            $page->save();
            \Flash::success('Modules copied!');
            return back();
        }

        \Flash::error('Error!');
        return back();
    }

    public function getIp()
    {
        foreach (
            array(
                'HTTP_CLIENT_IP',
                'HTTP_X_FORWARDED_FOR',
                'HTTP_X_FORWARDED',
                'HTTP_X_CLUSTER_CLIENT_IP',
                'HTTP_FORWARDED_FOR',
                'HTTP_FORWARDED',
                'REMOTE_ADDR'
            ) as $key) {
            if (array_key_exists($key, $_SERVER) === true) {
                foreach (explode(',', $_SERVER[$key]) as $ip) {
                    $ip = trim($ip); // just to be safe
                    return $ip;
                }
            }
        }
    }

    function country_code_to_locale($country_code, $language_code = '')
    {
        // Locale list taken from:
        // http://stackoverflow.com/questions/3191664/
        // list-of-all-locales-and-their-short-codes
        $locales = [
            'af-ZA',
            'am-ET',
            'ar-AE',
            'ar-BH',
            'ar-DZ',
            'ar-EG',
            'ar-IQ',
            'ar-JO',
            'ar-KW',
            'ar-LB',
            'ar-LY',
            'ar-MA',
            'arn-CL',
            'ar-OM',
            'ar-QA',
            'ar-SA',
            'ar-SY',
            'ar-TN',
            'ar-YE',
            'as-IN',
            'az-Cyrl-AZ',
            'az-Latn-AZ',
            'ba-RU',
            'be-BY',
            'bg-BG',
            'bn-BD',
            'bn-IN',
            'bo-CN',
            'br-FR',
            'bs-Cyrl-BA',
            'bs-Latn-BA',
            'ca-ES',
            'co-FR',
            'cs-CZ',
            'cy-GB',
            'da-DK',
            'de-AT',
            'de-CH',
            'de-DE',
            'de-LI',
            'de-LU',
            'dsb-DE',
            'dv-MV',
            'el-GR',
            'en-029',
            'en-AU',
            'en-BZ',
            'en-CA',
            'en-GB',
            'en-IE',
            'en-IN',
            'en-JM',
            'en-MY',
            'en-NZ',
            'en-PH',
            'en-SG',
            'en-TT',
            'en-US',
            'en-ZA',
            'en-ZW',
            'es-AR',
            'es-BO',
            'es-CL',
            'es-CO',
            'es-CR',
            'es-DO',
            'es-EC',
            'es-ES',
            'es-GT',
            'es-HN',
            'es-MX',
            'es-NI',
            'es-PA',
            'es-PE',
            'es-PR',
            'es-PY',
            'es-SV',
            'es-US',
            'es-UY',
            'es-VE',
            'et-EE',
            'eu-ES',
            'fa-IR',
            'fi-FI',
            'fil-PH',
            'fo-FO',
            'fr-BE',
            'fr-CA',
            'fr-CH',
            'fr-FR',
            'fr-LU',
            'fr-MC',
            'fy-NL',
            'ga-IE',
            'gd-GB',
            'gl-ES',
            'gsw-FR',
            'gu-IN',
            'ha-Latn-NG',
            'he-IL',
            'hi-IN',
            'hr-BA',
            'hr-HR',
            'hsb-DE',
            'hu-HU',
            'hy-AM',
            'id-ID',
            'ig-NG',
            'ii-CN',
            'is-IS',
            'it-CH',
            'it-IT',
            'iu-Cans-CA',
            'iu-Latn-CA',
            'ja-JP',
            'ka-GE',
            'kk-KZ',
            'kl-GL',
            'km-KH',
            'kn-IN',
            'kok-IN',
            'ko-KR',
            'ky-KG',
            'lb-LU',
            'lo-LA',
            'lt-LT',
            'lv-LV',
            'mi-NZ',
            'mk-MK',
            'ml-IN',
            'mn-MN',
            'mn-Mong-CN',
            'moh-CA',
            'mr-IN',
            'ms-BN',
            'ms-MY',
            'mt-MT',
            'nb-NO',
            'ne-NP',
            'nl-BE',
            'nl-NL',
            'nn-NO',
            'nso-ZA',
            'oc-FR',
            'or-IN',
            'pa-IN',
            'pl-PL',
            'prs-AF',
            'ps-AF',
            'pt-BR',
            'pt-PT',
            'qut-GT',
            'quz-BO',
            'quz-EC',
            'quz-PE',
            'rm-CH',
            'ro-RO',
            'ru-RU',
            'rw-RW',
            'sah-RU',
            'sa-IN',
            'se-FI',
            'se-NO',
            'se-SE',
            'si-LK',
            'sk-SK',
            'sl-SI',
            'sma-NO',
            'sma-SE',
            'smj-NO',
            'smj-SE',
            'smn-FI',
            'sms-FI',
            'sq-AL',
            'sr-Cyrl-BA',
            'sr-Cyrl-CS',
            'sr-Cyrl-ME',
            'sr-Cyrl-RS',
            'sr-Latn-BA',
            'sr-Latn-CS',
            'sr-Latn-ME',
            'sr-Latn-RS',
            'sv-FI',
            'sv-SE',
            'sw-KE',
            'syr-SY',
            'ta-IN',
            'te-IN',
            'tg-Cyrl-TJ',
            'th-TH',
            'tk-TM',
            'tn-ZA',
            'tr-TR',
            'tt-RU',
            'tzm-Latn-DZ',
            'ug-CN',
            'uk-UA',
            'ur-PK',
            'uz-Cyrl-UZ',
            'uz-Latn-UZ',
            'vi-VN',
            'wo-SN',
            'xh-ZA',
            'yo-NG',
            'zh-CN',
            'zh-HK',
            'zh-MO',
            'zh-SG',
            'zh-TW',
            'zu-ZA'
        ];

        foreach ($locales as $locale) {
            $locale_region = locale_get_region($locale);
            $locale_language = locale_get_primary_language($locale);
            $locale_array = array('language' => $locale_language,
                'region' => $locale_region);

            if (strtoupper($country_code) == $locale_region &&
                $language_code == '') {
                return mb_substr(locale_compose($locale_array), 0, 2);
            }

            if (strtoupper($country_code) == $locale_region &&
                strtolower($language_code) == $locale_language) {
                return mb_substr(locale_compose($locale_array), 0, 2);
            }
        }

        return null;
    }
}
