<?php namespace Bitcraft\Pagebuilder\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Bitcraft\Pagebuilder\Models\Menu;
use RainLab\Translate\Models\Locale;

class Menus extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Bitcraft.Pagebuilder', 'main-menu-item-pagebuilder', 'side-menu-item-menus');
    }

    public function onCopyLanguageItems($id)
    {
        $locales = explode(' -> ', strtolower(post('mode')));
        if (count($locales) === 2 && $menu = Menu::find($id)) {
            if ($locales[1] === Locale::getDefault()->code) {
                $menu->items = $menu->getAttributeTranslated('items', $locales[0]);
            } else {
                $menu->setAttributeTranslated(
                    'items',
                    $menu->getAttributeTranslated('items', $locales[0]),
                    $locales[1]
                );
            }

            $menu->save();
            \Flash::success('Items copied!');
            return back();
        }

        \Flash::error('Error!');
        return back();
    }
}
