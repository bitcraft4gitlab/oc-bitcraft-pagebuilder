<?php namespace Bitcraft\Pagebuilder\Models;

use Model;

/**
 * Model
 */
class Footer extends Model
{
    use \October\Rain\Database\Traits\Validation;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'bitcraft_pagebuilder_footers';
    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];
    protected $jsonable = ['social', 'links_left', 'links_right'];
    public $translatable = [
        'links',
        'image_alt',
        'image_title',
        'links_left',
        'links_right',
        'newsletter_title',
        'newsletter_subtitle',
        'logo_title',
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
