<?php namespace Bitcraft\Pagebuilder\Models;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManager;
use Model;
use RainLab\Translate\Models\Locale;
use Bitcraft\Seomanager\Models\SeoTag;

/**
 * Model
 */
class Page extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\SoftDelete;

    public $implement = [
        'RainLab.Translate.Behaviors.TranslatableModel',
        'Bitcraft.Versions.Behaviors.VersionsModelBehavior',
        'Bitcraft.Publish.Behaviors.PublishModelBehavior',
        'Bitcraft.Pagebuilder.Behaviors.ResizeImagesBehavior'
    ];

    protected $dates = ['deleted_at'];
    protected $jsonable = ['modules'];
    public $translatable = ['modules', ['slug', 'index' => true]];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'bitcraft_pagebuilder_pages';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'title' => 'required',
        'slug' => 'regex:/^[0-9a-zA-z\/\-\_]+$/',
    ];

    public $customMessages = [
        'slug.regex' => 'Slug allow only characters: A-Z,a-z,0-9,-,/,_',
    ];

    public $morphOne = [
        'seo_tag' => ['Bitcraft\Seomanager\Models\SeoTag', 'table' => 'bitcraft_seomanager_seo_tags', 'name' => 'seo_tag']
    ];

    public $belongsTo = [
        'menu' => 'Bitcraft\Pagebuilder\Models\Menu',
        'footer' => 'Bitcraft\Pagebuilder\Models\Footer',
        'platform' => 'Bitcraft\Publish\Models\Platform'
    ];

    public $resize_storage = 'local';
    public $frontend_sub_path = '';

    public function hasModules()
    {
        if (!empty($this->modules)) {
            return true;
        }

        foreach (array_keys(Locale::listAvailable()) as $locale) {
            if (!empty($this->getAttributeTranslated('modules', $locale))) {
                return true;
            }
        }
        return false;
    }

    public function duplicate()
    {
        //copy attributes
        $new = $this->replicate();
        $new->title = "$this->title (COPY)";
        $new->published = false;
        $new->slug = '';

        //save model
        $new->push();

        $locales = array_keys(Locale::listAvailable());

        // copy seo tag
        if ($this->seo_tag !== null) {
            $new_seo_tag = new SeoTag();

            // get translated values
            foreach ($locales as $locale) {
                $old_seo_tag = $this->seo_tag->noFallbackLocale()->lang($locale)->toArray();
                unset($old_seo_tag['id']);

                foreach ($old_seo_tag as $index => $item) {
                    $new_seo_tag->setAttributeTranslated($index, $item, $locale);
                }
            }
            $new_seo_tag->seo_tag_id = $new->id;
            $new_seo_tag->save();
        }

        $modules = [];
        // get translated values
        foreach ($locales as $locale) {
            if ($locale_modules = $this->getAttributeTranslated('modules', $locale)) {
                $modules[$locale] = $locale_modules;
            }
        }

        // save translated values to duplicated model
        foreach ($modules as $index => $locale_modules) {
            $new->setAttributeTranslated('modules', json_encode($locale_modules), $index);
        }

        $new->save();
    }

    public function beforeSave()
    {
        $regex = '/^[0-9a-zA-z\/\-\_]+$/';
        if (post("RLTranslate") !== null) {
            foreach (post("RLTranslate") as $index => $trans) {
                if (
                    !empty($trans['slug'])
                    && $trans['slug'] !== '/'
                    && substr($trans['slug'], -1) === '/'
                ) {
                    echo 'No trailing slash allowed for the path';
                    dd();
                }
                if (
                    !empty($trans['slug'])
                    && ($existing_page = self::transModelWhere($trans['slug'], $index, $this->platform_id))
                    && $existing_page->id !== $this->id
                ) {
                    echo "Page with this slug already exists. Page: $existing_page->title | Language: $index";
                    dd();
                }
                if (($slug = $trans['slug']) && !preg_match($regex, $slug)) {
                    echo 'Slug allow only characters: A-Z,a-z,0-9,-,/,_';
                    dd();
                }
            }
        }
        $this->updated_at = date('Y-m-d H:i:s');
    }

    public function afterSave()
    {
        $this->resizeAfterSave();
    }

    public function beforeDelete()
    {
        if ($this->hasPublishedVersion) {
            $this->handlePublishedPages();
            foreach ($this->versions as $version) {
                $version->delete();
            }
        }
    }
}
