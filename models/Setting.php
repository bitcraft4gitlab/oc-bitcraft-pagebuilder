<?php namespace Bitcraft\Pagebuilder\Models;

use Model;

/**
 * Model
 */
class Setting extends Model
{
    use \October\Rain\Database\Traits\Validation;

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];
    public $translatable = [
        'not_found_content',
        'facebook',
        'instagram',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'bitcraft_pagebuilder_settings';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'menu' => 'Bitcraft\Pagebuilder\Models\Menu',
        'footer' => 'Bitcraft\Pagebuilder\Models\Footer'
    ];
}
