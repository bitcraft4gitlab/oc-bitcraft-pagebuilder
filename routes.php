<?php

use Illuminate\Http\Request;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Facades\URL;
use October\Rain\Support\Facades\Config;
use RainLab\Translate\Models\Locale;
use RainLab\User\Facades\Auth;
use Illuminate\Support\Facades\Route;
use October\Rain\Support\Facades\Flash;
use Bitcraft\SeoManager\Classes\Robots;

Route::group([
    'middleware' => ['web'],
    'prefix' => Config::get('cms.backendUri', 'backend')
], function () {
    Route::any('{slug}', 'Backend\Classes\BackendController@run')->where('slug', '(.*)?');
});

Route::get('/{lang}/sitemap', function (Request $request, $lang) {
    $sitemap = App::make("sitemap");

    $pages = \Bitcraft\Pagebuilder\Models\Page::all();
    $locales = array_keys(Locale::listEnabled());

    foreach ($pages as $page) {
        if (($version = $page->currentModel($lang)) && !empty($version->slug) && $version->published) {
            $translations = [];
            foreach ($locales as $locale) {
                if ($locale === $lang) {
                    continue;
                }
                if (($locale_version = $page->currentModel($locale)) && !empty($locale_version->slug)) {
                    $translations[] = ['language' => $locale, 'url' => URL::to("/$locale$locale_version->slug")];
                }
            }

            $sitemap->add(URL::to("/$lang$version->slug"), null, '1.0', 'daily', [], null, $translations);
        }
    }

    return $sitemap->render('xml');
});

Route::get('/robots', function(Request $request) {
    $content = Robots::get();
    return response($content)->header('Content-Type', 'text/plain');
});

/*
 * Entry point
 */
Route::any(Config::get('cms.backendUri', 'backend'), 'Backend\Classes\BackendController@run')->middleware('web');

Route::get('/{any}', 'Bitcraft\Pagebuilder\Controllers\Pages@show')->where('any', '.*')->middleware('web');
