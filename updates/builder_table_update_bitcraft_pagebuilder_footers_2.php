<?php namespace Bitcraft\Pagebuilder\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBitcraftPagebuilderFooters2 extends Migration
{
    public function up()
    {
        Schema::table('bitcraft_pagebuilder_footers', function($table)
        {
            $table->text('links')->nullable();
        });
    }

    public function down()
    {
        Schema::table('bitcraft_pagebuilder_footers', function($table)
        {
            $table->dropColumn('links');
        });
    }
}
