<?php namespace Bitcraft\Pagebuilder\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBitcraftPagebuilderPages extends Migration
{
    public function up()
    {
        Schema::table('bitcraft_pagebuilder_pages', function($table)
        {
            $table->integer('footer_id')->unsigned();
        });
    }

    public function down()
    {
        Schema::table('bitcraft_pagebuilder_pages', function($table)
        {
            $table->dropColumn('footer_id');
        });
    }
}
