<?php namespace Bitcraft\Pagebuilder\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBitcraftPagebuilderMenus extends Migration
{
    public function up()
    {
        Schema::table('bitcraft_pagebuilder_menus', function($table)
        {
            $table->string('startpage_link')->nullable();
        });
    }

    public function down()
    {
        Schema::table('bitcraft_pagebuilder_menus', function($table)
        {
            $table->dropColumn('startpage_link');
        });
    }
}
