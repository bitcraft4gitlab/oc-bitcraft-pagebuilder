<?php namespace Bitcraft\Pagebuilder\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBitcraftPagebuilderSettings extends Migration
{
    public function up()
    {
        Schema::create('bitcraft_pagebuilder_settings', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->string('facebook')->nullable();
            $table->string('instagram')->nullable();
            $table->string('linkedin')->nullable();
            $table->text('not_found_content')->nullable();
            $table->integer('menu_id')->nullable()->unsigned();
            $table->integer('footer_id')->nullable()->unsigned();
        });
    }

    public function down()
    {
        Schema::dropIfExists('bitcraft_pagebuilder_settings');
    }
}
