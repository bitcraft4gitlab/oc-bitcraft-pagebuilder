<?php namespace Bitcraft\Pagebuilder\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBitcraftPagebuilderSettings2 extends Migration
{
    public function up()
    {
        Schema::table('bitcraft_pagebuilder_settings', function($table)
        {
            $table->text('css_head')->nullable();
            $table->text('head_script')->nullable();
            $table->text('body_script')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('bitcraft_pagebuilder_settings', function($table)
        {
            $table->dropColumn('css_head');
            $table->dropColumn('head_script');
            $table->dropColumn('body_script');
        });
    }
}
