<?php namespace Bitcraft\Pagebuilder\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBitcraftPagebuilderFooters3 extends Migration
{
    public function up()
    {
        Schema::table('bitcraft_pagebuilder_footers', function($table)
        {
            $table->text('newsletter_title')->nullable();
            $table->text('newsletter_subtitle')->nullable();
            $table->text('logo_title')->nullable();
        });
    }

    public function down()
    {
        Schema::table('bitcraft_pagebuilder_footers', function($table)
        {
            $table->dropColumn('newsletter_title');
            $table->dropColumn('newsletter_subtitle');
            $table->dropColumn('logo_title');
        });
    }
}
