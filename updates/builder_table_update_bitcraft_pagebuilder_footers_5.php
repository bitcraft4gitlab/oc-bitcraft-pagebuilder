<?php namespace Bitcraft\Pagebuilder\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBitcraftPagebuilderFooters5 extends Migration
{
    public function up()
    {
        Schema::table('bitcraft_pagebuilder_footers', function($table)
        {
            $table->dropColumn('image');
            $table->dropColumn('image_alt');
            $table->dropColumn('image_title');
            $table->dropColumn('newsletter_title');
            $table->dropColumn('newsletter_subtitle');
            $table->dropColumn('show_newsletter');
        });
    }
    
    public function down()
    {
        Schema::table('bitcraft_pagebuilder_footers', function($table)
        {
            $table->string('image', 191)->nullable();
            $table->text('image_alt')->nullable();
            $table->string('image_title', 191);
            $table->text('newsletter_title')->nullable();
            $table->text('newsletter_subtitle')->nullable();
            $table->boolean('show_newsletter')->nullable();
        });
    }
}
