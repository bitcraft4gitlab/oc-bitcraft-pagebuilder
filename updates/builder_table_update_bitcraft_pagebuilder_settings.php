<?php namespace Bitcraft\Pagebuilder\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBitcraftPagebuilderSettings extends Migration
{
    public function up()
    {
        Schema::table('bitcraft_pagebuilder_settings', function($table)
        {
            $table->string('title');
        });
    }
    
    public function down()
    {
        Schema::table('bitcraft_pagebuilder_settings', function($table)
        {
            $table->dropColumn('title');
        });
    }
}
